'use strict';

const links = require('./database.json')
    .map((l, index) => Object.assign({
        id: index + 1
    }, l, {
        pubDate: new Date(l.pubDate)
    }));
const pageSize = 30;
const Hapi = require('hapi');
const Inert = require('inert');
const Boom = require('boom');

const server = Hapi.server({
    port: process.env.PORT || 3000,
    host: '0.0.0.0',
    routes: {
        cors: {
            origin: ['*']
        }
    }
});

server.register(Inert)
    .then(() => {

        server.route({
            method: 'GET',
            path: '/link',
            handler: (request, h) => {
                const p = request.query.p;
                if (p && !p.match(/^[1-9]\d*$/))
                    throw Boom.badRequest("Request query parameter 'p' should be an integer greather than 0");
                const page = parseInt(request.query.p || 1);
                const start = (page - 1) * pageSize;
                const end = start + pageSize;
                return links.slice(start, end);
            }
        });

        server.route({
            method: 'POST',
            path: '/link',
            handler: (request, h) => {
                const body = request.payload;
                if (!body || Object.keys(body).length != 2 || !body.description || !body.url)
                    throw Boom.badRequest("Request payload should be an object containing only description and url");
                const link = {
                    description: body.description,
                    url: body.url,
                    pubDate: new Date(),
                    points: 0,
                    id: links.reduce((acc, l) => Math.max(acc, l.id), 0) + 1
                }
                links.splice(0, 0, link);
                return h.response(link).code(201);
            }
        });

        server.route({
            method: 'POST',
            path: '/link/{id}/upvote',
            handler: (request, h) => {
                const id = request.params.id;
                if (!id || !id.match(/^[1-9]\d*$/))
                    throw Boom.badRequest("Request parameter 'id' should be an integer greather than 0");
                const parsedId = parseInt(id);
                const link = links.find(l => l.id === parsedId);
                if (!link)
                    throw Boom.notFound("Request parameter 'id' does not match any existent link")
                link.points++;
                return h.response(link).code(201);
            }
        });

        server.route({
            method: 'GET',
            path: '/{param*}',
            handler: {
                directory: {
                    path: 'public'
                }
            }
        });
        
        const init = async () => {
            await server.start();
            console.log(`Server running at: ${server.info.uri}`);
        };
        
        process.on('unhandledRejection', (err) => {
            console.log(err);
            process.exit(1);
        });
        
        init();
    });